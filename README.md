# Mirror Monitor

Monitor the status of known mirrors of https://guardianproject.info/fdroid.

fingerprint: B7C2EEFD8DAC7806AF67DFCD92EB18126BC08312A7F2D6F3862E46013C7A6135

## Active Mirrors

* https://guardianproject.info/fdroid/repo
